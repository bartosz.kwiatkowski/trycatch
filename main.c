#include <stdio.h>
#include "try-catch.h"

int main(){

    int mianownik = 10;
    int licznik = 0;

    TRY (
        printf("Dzielenie = %d \n", mianownik / licznik);

    ) CATCH ( exception,
        printf("caught %d\n",exception); 
    ) FINALLY();

    puts("Program trwa dalej");

    return 0;
}
